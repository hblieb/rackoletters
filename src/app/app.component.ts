import { Component } from '@angular/core';

@Component({
  selector: 'rol-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'rol';
}
