import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAX_WORDS, RackService } from './rack.service';

export interface Criteria {
  rack: string;
  startsWith: string;
  contains: string;
  endsWith: string;
}

@Component({
  selector: 'rol-rack',
  templateUrl: './rack.component.html',
  styleUrls: ['./rack.component.scss'],
})
export class RackComponent implements OnInit, AfterViewInit {
  maxWords = MAX_WORDS;
  showHelp: boolean;
  rackForm: FormGroup;
  foundWords: string[] = [];
  browser: number;
  showWords: boolean;
  @ViewChild('rackField') rackField: any;

  constructor(private fb: FormBuilder, private service: RackService) {
    this.createForm();
    const stored = localStorage.getItem('showWords');
    switch (stored) {
      case 'false':
        this.showWords = false;
        break;
      case 'true':
        this.showWords = true;
        break;
      default:
        this.showWords = true;
        localStorage.setItem('showWords', 'true');
    }
  }

  createForm() {
    this.rackForm = this.fb.group({
      rack: '',
      startsWith: '',
      contains: '',
      endsWith: '',
    });
  }

  regexHasContents(field) {
    if (field) {
      return field.length > 0;
    }
    return false;
  }

  ngOnInit() {
    this.browser = detectIE();
  }

  ngAfterViewInit() {
    this.service.init();
  }

  searchRack() {
    const criteria: Criteria = this.rackForm.value;
    this.foundWords = this.service.searchRack(criteria);
  }

  toggleHelp() {
    this.showHelp = !this.showHelp;
  }

  // Clear the rack, set focus.
  clearAll() {
    this.rackForm.setValue({
      rack: '',
      startsWith: '',
      contains: '',
      endsWith: '',
    });
    this.foundWords = [];
    this.rackField.nativeElement.focus();
  }

  toggleWords(e) {
    if (e.checked) {
      localStorage.setItem('showWords', 'true');
      this.showWords = true;
    } else {
      localStorage.setItem('showWords', 'false');
      this.showWords = false;
    }
  }

  getInitial(val: string) {
    return val.substring(0, 1);
  }
}

function detectIE(): number {
  const ua = window.navigator.userAgent;

  // Test values; Uncomment to check result …

  // IE 10
  // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

  // IE 11
  // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

  // Edge 12 (Spartan)
  // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

  // Edge 13
  // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)
  // Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

  const msie = ua.indexOf('MSIE ');
  if (msie > 0) {
    // IE 10 or older => return version number
    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
  }

  const trident = ua.indexOf('Trident/');
  if (trident > 0) {
    // IE 11 => return version number
    const rv = ua.indexOf('rv:');
    return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
  }

  const edge = ua.indexOf('Edge/');
  if (edge > 0) {
    // Edge (IE 12+) => return version number
    // return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    return 0;
  }

  // other browser
  return 0;
}
