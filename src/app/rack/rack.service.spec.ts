import { getTestBed, TestBed } from '@angular/core/testing';

import { cleanTheRack, currentLetterAvailable, makeRegex, patternSupplied, RackService } from './rack.service';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('RackService', () => {
  let injector: TestBed;
  let service: RackService;
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ReactiveFormsModule,
        // HttpClientModule,
      ],
      providers: [FormBuilder, RackService],
    });
    injector = getTestBed();
    service = injector.get(RackService);
    httpMock = injector.get(HttpTestingController);
  });
  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('cleanTheRack method', () => {
    it('when passed undefined, should return a string', () => {
      const rack = undefined;
      const ret = cleanTheRack(rack);
      expect(typeof ret).toBe('string');
    });
    it('should return an empty string when passed an empty string', () => {
      const rack = '';
      const ret = cleanTheRack(rack);
      expect(ret).toBe('');
    });
    it('should return a string with no spaces', () => {
      const rack = 'Hello THE??RE';
      const ret = cleanTheRack(rack);
      expect(ret.indexOf(' ')).toEqual(-1);
    });
    it('should strip out non-letters', () => {
      const rack = 'H™e34l!lo THE??R9@E';
      const ret = cleanTheRack(rack);
      expect(ret).toEqual('hellothe??re');
    });
    it('should return empty string if passed no letters and no "?"s and no "."s ', () => {
      const rack = '™34!@';
      const ret = cleanTheRack(rack);
      expect(ret).toEqual('');
    });
  });

  describe('currentLetterAvailable', () => {
    it('should return true when a given letter is in the rack', () => {
      const letter = 'h';
      const rack = ['c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k'];
      const ret = currentLetterAvailable(letter, rack);
      expect(ret).toBe(true);
    });
    it('should return true when a "?" is in the rack', () => {
      const letter = 'h';
      const rack = ['c', 'd', 'e', 'f', '?', 'i', 'j', 'k'];
      const ret = currentLetterAvailable(letter, rack);
      expect(ret).toBe(true);
    });
    it('should return false when neither letter nor "?" are in the rack', () => {
      const letter = 'h';
      const rack = ['c', 'd', 'e', 'f', 'g', 'i', 'j', 'k'];
      const ret = currentLetterAvailable(letter, rack);
      expect(ret).toBe(false);
    });
  });

  describe('patternSupplied', () => {
    it('should return false on a newly initialized form', () => {
      const criteria = {
        rack: '',
        startsWith: '',
        contains: '',
        endsWith: '',
      };
      const ret = patternSupplied(criteria);
      expect(ret).toBe(false);
    });
    it('should return false when criteria only has spaces', () => {
      const criteria = {
        rack: '',
        startsWith: ' ',
        contains: '        ',
        endsWith: '    ',
      };
      const ret = patternSupplied(criteria);
      expect(ret).toBe(false);
    });
    it('should return true when a criteria element contains any letter', () => {
      const criteria = {
        rack: '',
        startsWith: 'a',
        contains: '        ',
        endsWith: '    ',
      };
      const ret = patternSupplied(criteria);
      expect(ret).toBe(true);
    });
    it('should return false when a criteria element contains any letter', () => {
      const criteria = {
        rack: '',
        startsWith: '4',
        contains: '        ',
        endsWith: '    ',
      };
      const ret = patternSupplied(criteria);
      expect(ret).toBe(false);
    });
  });

  describe('make regex method', () => {
    beforeEach(() => {
      this.fb = new FormBuilder();
    });

    it('should return a regular expression', () => {
      const criteria = {
        rack: '',
        startsWith: 'abr',
        contains: 'cad',
        endsWith: 'bra',
      };
      const result = makeRegex(criteria);
      expect(Object.prototype.toString.call(result)).toEqual('[object RegExp]');
    });
    it('should return a lowercase regular expression', () => {
      const criteria = {
        rack: '',
        startsWith: 'ABR',
        contains: 'cAd',
        endsWith: 'Bra',
      };
      const result = makeRegex(criteria);
      expect(result).toEqual(/^abr.*?cad.*?bra$/);
    });
    it('should return a expected regex if only startsWith is defined', () => {
      const criteria = {
        rack: '',
        startsWith: 'abr',
        contains: '',
        endsWith: '',
      };
      const result = makeRegex(criteria);
      expect(result).toEqual(/^abr.*?.*?$/);
    });
    it('should return a expected regex if only middle is defined', () => {
      const criteria = {
        rack: '',
        startsWith: '',
        contains: 'cad',
        endsWith: '',
      };
      const result = makeRegex(criteria);
      expect(result).toEqual(/^.*?cad.*?$/);
    });
    it('should return a expected regex if only endsWith is defined', () => {
      const criteria = {
        rack: '',
        startsWith: '',
        contains: '',
        endsWith: 'bra',
      };
      const result = makeRegex(criteria);
      expect(result).toEqual(/^.*?.*?bra$/);
    });
    it('should return a expected regex startsWith contains a placeholder (.)', () => {
      const criteria = {
        rack: '',
        startsWith: 'b.r',
        contains: '',
        endsWith: '',
      };
      const result = makeRegex(criteria);
      expect(result).toEqual(/^b.r.*?.*?$/);
    });
    it('should return a expected regex with brackets', () => {
      const criteria = {
        rack: '',
        startsWith: 'b.r',
        contains: '',
        endsWith: '[nt]',
      };
      const result = makeRegex(criteria);
      expect(result).toEqual(/^b.r.*?.*?[nt]$/);
    });
  });
  describe('regex wildcards with regex method', () => {
    beforeEach(() => {
      this.fb = new FormBuilder();
    });
    it('should use brackets for alternative letters', function() {
      const criteria = {
        rack: '',
        startsWith: '[ab]',
        contains: '',
        endsWith: '',
      };
      const result = makeRegex(criteria);
      expect(result).toEqual(/^[ab].*?.*?$/);
    });
  });
});
