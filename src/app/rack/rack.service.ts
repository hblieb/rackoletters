import { Injectable } from '@angular/core';
import { Criteria } from './rack.component';
import { HttpClient } from '@angular/common/http';

export const MAX_WORDS = 200;

@Injectable()
export class RackService {
  dictionary: string[];

  constructor(private http: HttpClient) {}

  init() {
    this.http
      .get('./assets/word-list.json')
      .subscribe((data: { words: string[] }) => (this.dictionary = data.words));
  }

  searchRack(criteria: Criteria): string[] {
    const foundWords = [];
    const rack = cleanTheRack(criteria.rack);
    if (rack.length < 2) {
      return;
    }
    let regex: RegExp;

    // Make a regex, if any patterns are supplied
    if (patternSupplied(criteria)) {
      regex = makeRegex(criteria);
    }

    let i = 0;
    const dl = this.dictionary.length;
    for (i; i < dl; i++) {
      const currentWord = this.dictionary[i];
      const currentWordLength = currentWord.length;
      const rackAsArray = rack.length > 1 ? rack.split('') : [''];

      // if currentWord is longer than rack, skip to next word.
      if (wordIsTooLong(currentWord, rack)) {
        continue; // skip to next word in dictionary
      }
      // if regex doesn't match, skip to next word.
      if (regex && !regexMatches(currentWord, regex)) {
        continue; // skip to next word in dictionary
      }

      for (let j = 0; j < currentWordLength; j++) {
        const currentLetter = currentWord[j];

        // commit letters from our rack, and wildcards, as needed
        if (letterFoundInRack(currentLetter, rackAsArray)) {
          rackAsArray.splice(rackAsArray.indexOf(currentLetter), 1);
        } else if (wildCardAvailable(rackAsArray)) {
          rackAsArray.splice(rackAsArray.indexOf('?'), 1);
        } else {
          break;
        }

        // Covered all letters in the word? That's a "found" word.
        if (noMoreLetters(j, currentWord)) {
          foundWords.push(currentWord);
        }
      }
    }
    return sortResults(foundWords);
  }
}

// return lowercase rack, only letters, always defined, ignore empty rack.
export function cleanTheRack(rack: string = ''): string {
  if (!rack || rack.length === 0) {
    return '';
  }
  rack = rack.toLowerCase().replace(/[^a-zA-Z\?\.\[\]]/g, '');
  return rack;
}

function noMoreLetters(i: number, word: string): boolean {
  return i + 1 === word.length;
}

export function currentLetterAvailable(
  letter: string,
  rack: string[]
): boolean {
  return letterFoundInRack(letter, rack) || wildCardAvailable(rack);
}

function letterFoundInRack(letter: string, rack: string[]): boolean {
  return rack.indexOf(letter) !== -1;
}

function wildCardAvailable(rack: string[]): boolean {
  return rack.indexOf('?') !== -1;
}

export function makeRegex(criteria: Criteria): RegExp {
  const s = cleanTheRack(criteria.startsWith);
  const c = cleanTheRack(criteria.contains);
  const e = cleanTheRack(criteria.endsWith);
  return new RegExp('^' + s + '.*?' + c + '.*?' + e + '$');
}

// Did the user supply any "startsWith", "contains", or "endsWith" values?
export function patternSupplied(criteria: Criteria): boolean {
  const s = cleanTheRack(criteria.startsWith);
  const c = cleanTheRack(criteria.contains);
  const e = cleanTheRack(criteria.endsWith);
  return !!(s || c || e);
}

function wordIsTooLong(word: string, rack: string): boolean {
  return word.length > rack.length;
}

function regexMatches(word: string, regex: RegExp): boolean {
  return regex && regex.test(word);
}

function sortResults(arr: string[]): string[] {
  const allSorted = arr.sort(function(a, b) {
    if (a.length < b.length) {
      return 1;
    } else if (a.length > b.length) {
      return -1;
    } else {
      return a < b ? 1 : -1;
    }
  });
  return allSorted.slice(0, MAX_WORDS);
}
